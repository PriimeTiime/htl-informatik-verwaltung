/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.test;

import at.htlpinkafeld.htlplusinformatik.dto.Mitglied;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author danie
 */
public class MitgliedTest {
    private static Mitglied instance;
    
    public MitgliedTest(){}
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() {
        instance = new Mitglied("Herbert", "Grabner", "Obmann",
                "aaa@a.at", "0664/1234567");
    }
    
    @After
    public void tearDown() {
        
    }
    
    @Test
    public void testBildungspartner() {
        System.out.println("testBildungspartner");
        
        Mitglied expResult = new Mitglied("Herbert", "Grabner", "Obmann","aaa@a.at", "0664/1234567");
        Mitglied result = instance;
        
        assertEquals(expResult, result);
    }
}
