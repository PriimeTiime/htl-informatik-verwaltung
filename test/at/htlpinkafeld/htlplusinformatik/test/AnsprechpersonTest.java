/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.test;

import at.htlpinkafeld.htlplusinformatik.dto.Ansprechperson;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andor
 */
public class AnsprechpersonTest {
    private static Ansprechperson instance;
    
    public AnsprechpersonTest() { }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Ansprechperson("Carsten Wenger", "Verkauf", "carsten.wenger@gmail.com", "+43 345 6789",3,"Internet");
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAnsprechperson() {
        System.out.println("testAnsprechperson");
        
        Ansprechperson expResult = new Ansprechperson("Carsten Wenger", "Verkauf",
                "carsten.wenger@gmail.com", "+43 345 6789",3,"Internet");
        Ansprechperson result = instance;
        
        assertEquals(expResult, result);
    }
}
