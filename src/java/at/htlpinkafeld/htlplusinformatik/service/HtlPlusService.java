/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.service;

import at.htlpinkafeld.htlplusinformatik.dto.Ansprechperson;
import at.htlpinkafeld.htlplusinformatik.dto.BPJahr;
import at.htlpinkafeld.htlplusinformatik.dto.Benutzer;
import at.htlpinkafeld.htlplusinformatik.dto.Bildungspartner;
import at.htlpinkafeld.htlplusinformatik.dto.Jahr;
import at.htlpinkafeld.htlplusinformatik.dto.Mitglied;
import at.htlpinkafeld.htlplusinformatik.infrastructure.AnsprechpersonDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.AnsprechpersonJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BPJahrDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BPJahrJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BenutzerDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BenutzerJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BildungspartnerDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BildungspartnerJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.JahrDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.JahrJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.MitgliedDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.MitgliedJdbcDao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author danie
 */
@ManagedBean
@SessionScoped
public class HtlPlusService {
    private BildungspartnerDao daoB;
    private MitgliedDao daoM;
    private BenutzerDao daoBen;
    private JahrDao daoJ;
    private BPJahrDao daoBpj;
    private AnsprechpersonDao daoA;
    private List<Bildungspartner> filteredBpList;
    
    public HtlPlusService() {
        this.daoB = new BildungspartnerJdbcDao("Bildungspartner", "BPNum");
        this.daoM = new MitgliedJdbcDao("Mitglieder", "MitgliedID");
        this.daoBen = new BenutzerJdbcDao("Benutzer", "UserID");
        this.daoJ = new JahrJdbcDao("Jahr","JahrID");
        this.daoBpj = new BPJahrJdbcDao("BP_Jahr", "BPJahrID");
        this.daoA = new AnsprechpersonJdbcDao("Ansprechperson", "AnsPersID");
        filteredBpList = daoB.list();
    }

    public List<Bildungspartner> getBpList() {
        return daoB.list();
    }

    public BaseDao<Bildungspartner> getBDao() {
        return daoB;
    }

    public void setBDao(BildungspartnerDao dao) {
        this.daoB = dao;
    }
    
    public void addB(Bildungspartner bp){
        daoB.insert(bp);
    }
    
    public void removeB(Bildungspartner bp){
        daoB.delete(bp);
    }
    
    public void updateB(Bildungspartner bp){
        daoB.update(bp);
    }

    public List<Bildungspartner> getFilteredBpList() {
        return filteredBpList = daoB.list();
    }

    public void setFilteredBpList(List<Bildungspartner> filteredList) {
        this.filteredBpList = filteredList;
    }
    
       
    //--------------------
    public List<Mitglied> getMList() {
        return daoM.list();
    }

    public BaseDao<Mitglied> getMDao() {
        return daoM;
    }

    public void setMDao(MitgliedDao dao) {
        this.daoM = dao;
    }
    
    public void addM(Mitglied m){
        daoM.insert(m);
    }
    
    public void removeM(Mitglied m){
        daoM.delete(m);
    }
    
    public void updateM(Mitglied m){
        daoM.update(m);
    } 
    
    //--------------------
    public List<Jahr> getJList() {
        return daoJ.list();
    }

    public BaseDao<Jahr> getJDao() {
        return daoJ;
    }

    public void setJDao(JahrDao dao) {
        this.daoJ = dao;
    }
    
    public void addJ(Jahr j){
        daoJ.insert(j);
    }
    
    public void removeJ(Jahr j){
        daoJ.delete(j);
    }
    
    public void updateJ(Jahr j){
        daoJ.update(j);
    }    
    
    
    //-----------------
    
    public List<Benutzer> getBenList() {
        return daoBen.list();
    }

    public BaseDao<Benutzer> getBenDao() {
        return daoBen;
    }

    public void setBenDao(BenutzerDao dao) {
        this.daoBen = dao;
    }
    
    public void addBenutzer(Benutzer b){
        if(!daoBen.list().stream().filter(o -> o.getName()
                .equals(b.getName())).findFirst().isPresent()) //Prüfen, ob es schon einen Benutzer
                                                        //mit diesem Namen gibt.
            daoBen.insert(b);
    }
    
    public Benutzer selectBenutzer(String username) {
        return daoBen.selectBenutzer(username);
    }
    
    public void removeBenutzer(Benutzer b){
        daoBen.delete(b);
    }
    
    public void updateBenutzer(Benutzer b){
        daoBen.update(b);
    }   
    
    //------------------------------------------------------------
    public List<BPJahr> getBPJList() {
        return daoBpj.list();
    }

    public BPJahrDao getDaoBpj() {
        return daoBpj;
    }

    public void setDaoBpj(BPJahrDao daoBpj) {
        this.daoBpj = daoBpj;
    }
    
    public void addBPJ(BPJahr bj){
        daoBpj.insert(bj);
    }
    
    public void removeBPJ(BPJahr bj){
        daoBpj.delete(bj);
    }
    
    public void updateBPJ(BPJahr bj){
        daoBpj.update(bj);
    }  
    
    //------------------------------------------------------------
    public List<Ansprechperson> getAList() {
        return daoA.list();
    }

    public AnsprechpersonDao getDaoA() {
        return daoA;
    }

    public void setDaoA(AnsprechpersonDao daoA) {
        this.daoA = daoA;
    }
    
    public void addA(Ansprechperson a){
        daoA.insert(a);
    }
    
    public void removeA(Ansprechperson a){
        daoA.delete(a);
    }
    
    public void updateA(Ansprechperson a){
        daoA.update(a);
    }  
    
}
