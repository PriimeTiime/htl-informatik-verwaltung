/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Jahr;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.JahrJdbcDao;
import static at.htlpinkafeld.htlplusinformatik.presentation.BildungspartnerBean.logger;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class JahrBean implements Serializable{

    public static Logger logger = Logger.getLogger(JahrBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private Jahr j = new Jahr();
    private BaseDao<Jahr> dao = new JahrJdbcDao("Jahr", "JahrID");
    private List<Jahr> jList = service.getJList();
    /**
     * Creates a new instance of JahrBean
     */
    public JahrBean() {
        logger.log(Level.DEBUG, "Created instance of JahrBean");
    }
    
    public HtlPlusService getService() {
        return service;
    }
    
    public void setService(HtlPlusService service) {
        this.service = service;
    }
    
    public Jahr getJahr(){
        return j;
    }
    
    public String saveJahr(){
        service.updateJ(j);
        logger.log(Level.INFO, "Jahr changed");
        return "success";
    }
    
    public String editBildungspartner(Jahr j){
        FacesContext.getCurrentInstance().renderResponse();
        j.setId(j.getId());
        j.setJahrbez(j.getJahrbez());
        return "successEdit";
    }
    
    public String deleteJahr(Jahr j) {
        service.removeJ(j);
        logger.log(Level.INFO, "Jahr deleted");
        return "successDelete";
    }
    
    public String addJahr(){
        service.addJ(j);
        logger.log(Level.INFO, "Jahr added");
        j = new Jahr();
        return "success";
    }
    
    public String setEmptyBildungspartner(){
        j = new Jahr();
        return "successAdd";
    }
    
    
}
