/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Bildungspartner;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BildungspartnerJdbcDao;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.lowagie.text.Paragraph;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

  



import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
@ManagedBean
@SessionScoped
@ViewScoped
public class BildungspartnerBean implements Serializable {
    public static Logger logger = Logger.getLogger(BildungspartnerBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private Bildungspartner bp = new Bildungspartner();
    private BaseDao<Bildungspartner> dao = new BildungspartnerJdbcDao("Bildungspartner", "BPNum");
    private List<Bildungspartner> bpList = service.getBpList();
    
    public BildungspartnerBean() {
        logger.log(Level.DEBUG, "Created instance of BildungspartnerBean");
    }

    public HtlPlusService getService() {
        return service;
    }

    public void setService(HtlPlusService service) {
        this.service = service;
    }

    public Bildungspartner getBp() {
        return bp;
    }
    
    public String saveBildungspartner(){
        service.updateB(bp);
        logger.log(Level.INFO, "Bildungspartner changed");
        return "success";
    }
    
    public String editBildungspartner(Bildungspartner b){
        FacesContext.getCurrentInstance().renderResponse();
        bp.setId(b.getId());
        bp.setFirma(b.getFirma());
        //bp.setAnsprPers(b.getAnsprPers());
        bp.setAddr(b.getAddr());
        bp.setPlz(b.getPlz());
        bp.setOrt(b.getOrt());
        bp.setAbsolvent(b.isAbsolvent());
        bp.setWebsite(b.getWebsite());
        bp.setFax(b.getFax());
        bp.seteDatum(b.geteDatum());
        bp.setaDatum(b.getaDatum());
        bp.setAnmerk(b.getAnmerk());
        return "successEdit";
    }
    
    public String deleteBildungspartner(Bildungspartner bp) {
        service.removeB(bp);
        logger.log(Level.INFO, "Bildungspartner deleted");
        return "successDelete";
    }
    
    public String addBildungspartner() {
        service.addB(bp);
        logger.log(Level.INFO, "Bildungspartner added");
        bp = new Bildungspartner();
        return "success";
    }
    
    public String setEmptyBildungspartner(){
        bp = new Bildungspartner();
        return "successAdd";
    }
    
    public Object createRechnung(Bildungspartner bp){
        try{
            DateFormat dF;
            Date date;
            dF = new SimpleDateFormat("dd.MM.yyyy");
            date = new Date();
            
            
            com.lowagie.text.Document document = new com.lowagie.text.Document();
            com.lowagie.text.pdf.PdfWriter.getInstance(document, new FileOutputStream("d:/Rechnung "+bp.getFirma()+".pdf"));
            document.open();
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph("Verein HTL PLUS-Informatik                                                   Rechnung Nr.                           "+1 +"/19"));
            document.add(new Paragraph("                                                                                                 Rechnungsdatum          "+dF.format(date)));
            document.add(new Paragraph(bp.getFirma()));
            document.add(new Paragraph(bp.getAddr()));
            document.add(new Paragraph("A-" + bp.getPlz()+ " " + bp.getOrt()));
            document.add(new Paragraph(" "));
            document.add(new Paragraph("Rechnung " + 1 +"/19"));
            document.add(new Paragraph(" "));
            
            /*com.lowagie.text.pdf.PdfPTable table = new com.lowagie.text.pdf.PdfPTable(2);
            table.setWidthPercentage(100);
            table.setSpacingAfter(10f);
            table.setSpacingBefore(15f);
            table.setWidths(new float[] { 2f, 2f });
            
            table.addCell("Data 1");
            table.addCell("Data 2");
            table.addCell("Data 3");
            table.addCell("Data 4");
            
            document.add(table);*/
            document.add(new Paragraph("Bezeichnung                                                                                                 Betrag"));
            document.add(new Paragraph("Zahlung Bildungspartnerbeitrag                                                                      "+bp.getBetrag()+"€"));
            document.add(new Paragraph("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n "));
            document.add(new Paragraph("____________________________________________________________________________"));
            document.add(new Paragraph("Raiba Pinkafeld\n" +
                                       "BIC: RLBBAT2E125\n" +
                                       "IBAN: AT20 3312 5001 0070 4445 "));
            
            document.close(); 
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
}

