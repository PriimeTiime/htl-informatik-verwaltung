/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Benutzer.Right;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Andor
 */
@ManagedBean
@SessionScoped
public class RightBean {
    public SelectItem[] getRightValues() {
        SelectItem[] items = new SelectItem[Right.values().length];
        int i = 0;

        for (Right r : Right.values()) {
            items[i++] = new SelectItem(r, r.toString());
        }

        return items;
    }
}
