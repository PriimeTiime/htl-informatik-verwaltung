/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Mitglied;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.MitgliedJdbcDao;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
@ManagedBean
@SessionScoped
public class MitgliedBean implements Serializable{
    public static Logger logger = Logger.getLogger(MitgliedBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private Mitglied m = new Mitglied();
    private BaseDao<Mitglied> dao = new MitgliedJdbcDao("Mitglieder", "MitgliedID");
    private List<Mitglied> mList = service.getMList();
    
    public MitgliedBean() {
        logger.log(Level.DEBUG, "Created instance of MitgliedBean");
    }

    public HtlPlusService getService() {
        return service;
    }

    public void setService(HtlPlusService service) {
        this.service = service;
    }

    public Mitglied getM() {
        return m;
    }
    
    public String saveMitglied(){
        service.updateM(m);
        logger.log(Level.INFO, "Mitglied changed");
        return "success";
    }
    
    public String editMitglied(Mitglied mi){  
        FacesContext.getCurrentInstance().renderResponse();
        m.setId(mi.getId());
        m.setVname(mi.getVname());
        m.setNname(mi.getNname());
        m.setFunktion(mi.getFunktion());
        m.setEmail(mi.getEmail());
        m.setTelnr(mi.getTelnr());
        return "successEdit";
    }
    
    public String deleteMitglied(Mitglied m) {
        service.removeM(m);
        logger.log(Level.INFO, "Mitglied deleted");
        return "successDelete";
    }
    
    public String addMitglied(){
        service.addM(m);
        logger.log(Level.INFO, "Mitglied added");
        m = new Mitglied();
        return "success";
    }
    
    public String setEmptyMitglied(){
        m = new Mitglied();
        return "successAdd";
    }
    
}
