/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Ansprechperson;
import at.htlpinkafeld.htlplusinformatik.infrastructure.AnsprechpersonJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
@ManagedBean
@RequestScoped
@ViewScoped
public class AnsprechpersonBean implements Serializable{

    public static Logger logger = Logger.getLogger(AnsprechpersonBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private Ansprechperson a = new Ansprechperson();
    private BaseDao<Ansprechperson> dao = new AnsprechpersonJdbcDao("Bildungspartner", "BPNum");
    private List<Ansprechperson> aList = service.getAList();
    /**
     * Creates a new instance of AnsprechpersonBean
     */
    public AnsprechpersonBean() {
        logger.log(Level.DEBUG, "Created instance of AnsprechpersonBean");
    }

    public HtlPlusService getService() {
        return service;
    }

    public void setService(HtlPlusService service) {
        this.service = service;
    }

    public Ansprechperson getA() {
        return a;
    }
    
    public String saveAnsprechperson(){
        service.updateA(a);
        logger.log(Level.INFO, "Ansprechperson changed");
        return "success";
    }
    
    public String editAnsprechperson(Ansprechperson an){
        FacesContext.getCurrentInstance().renderResponse();
        a.setId(an.getId());
        a.setName(an.getName());
        a.setAbt(an.getAbt());
        a.setEmail(an.getEmail());
        a.setTel(an.getTel());
        a.setBpId(an.getBpId());
        a.setZuständikeit(an.getZuständikeit());
        return "successEdit";
    }
    
    public String deleteAnsprechperson(Ansprechperson an) {
        service.removeA(an);
        logger.log(Level.INFO, "Ansprechperson deleted");
        return "successDelete";
    }
    
    public String addAnsprechperson() {
        service.addA(a);
        logger.log(Level.INFO, "Ansprechperson added");
        a = new Ansprechperson();
        return "success";
    }
    
    public String setEmptyAnsprechperson(){
        a = new Ansprechperson();
        return "successAdd";
    }
    
}
