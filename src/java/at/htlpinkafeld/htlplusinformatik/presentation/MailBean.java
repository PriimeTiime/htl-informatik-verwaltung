/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Bildungspartner;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Andor
 */
public class MailBean {

    HtlPlusService service;
    private List<Bildungspartner> bpList = new ArrayList<>();

    private String recipients;
    private String allRecipients;

    private final String USERNAME = "htlplus@outlook.com";
    private final String PASSWORD = "bC9qe4FH";
    
    private String betreff;
    private String inhalt;

    public Object sendRemindMessage() {
        bpList = service.getBpList();

        for (int x = 0; x < service.getBpList().size() - 1; x++) {
            if (!service.getBpList().get(x).isBezahlt()) {
                recipients += service.getBpList().get(x).getEmail() + ", ";
            }
        }

        Properties props = new Properties();
        props.put("mail.smtp.user", USERNAME);
        props.put("mail.smtp.host", "smtp-mail.outlook.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "true");

        try {
            Authenticator auth = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(USERNAME, PASSWORD);
                }
            };

            Session session = Session.getInstance(props, auth);

            MimeMessage msg = new MimeMessage(session);
            msg.setText("Liebe/r Herr/Frau, wir möchten Sie daran erinnern, den ausständigen Betrag, "
                    + "der zu bezahlen ist, so früh wie möglich zu überweisen.");
            msg.setSubject("Erinnerungs-Mail bezüglich den ausständigen Beträge");
            msg.setFrom(new InternetAddress(USERNAME));
            msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
            Transport.send(msg);

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

        return null;
    }

    public Object sendInformationMessage() {
        for(int x = 0; x < service.getBpList().size() - 1; x++) {
            allRecipients += service.getBpList().get(x) + ", ";
        }
                
        Properties props = new Properties();
        props.put("mail.smtp.user", USERNAME);
        props.put("mail.smtp.host", "smtp-mail.outlook.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "true");

        try {
            Authenticator auth = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(USERNAME, PASSWORD);
                }
            };

            Session session = Session.getInstance(props, auth);

            MimeMessage msg = new MimeMessage(session);
            msg.setText("");
            msg.setSubject("");
            msg.setFrom(new InternetAddress(USERNAME));
            msg.addRecipients(Message.RecipientType.TO, InternetAddress.parse(allRecipients));
            Transport.send(msg);

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

        return null;
    }

    public List<Bildungspartner> getBpList() {
        return bpList;
    }

    public void setBpList(List<Bildungspartner> bpList) {
        this.bpList = bpList;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getBetreff() {
        return betreff;
    }

    public void setBetreff(String betreff) {
        this.betreff = betreff;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }

    
    
}
