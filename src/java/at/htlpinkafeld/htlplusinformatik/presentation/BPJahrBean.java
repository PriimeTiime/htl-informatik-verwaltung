/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.BPJahr;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BPJahrJdbcDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
@ManagedBean
@RequestScoped
@ViewScoped
public class BPJahrBean implements Serializable{
    
    public static Logger logger = Logger.getLogger(BPJahrBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private BPJahr bpJ = new BPJahr();
    private BaseDao<BPJahr> dao = new BPJahrJdbcDao("BP_Jahr", "BPJahrID");
    private List<BPJahr> bpJList = service.getBPJList();

    /**
     * Creates a new instance of BPJahrBean
     */
    public BPJahrBean() {
        logger.log(Level.DEBUG, "Created instance of BPJahrBean");
    }
    
    public HtlPlusService getService() {
        return service;
    }

    public void setService(HtlPlusService service) {
        this.service = service;
    }

    public BPJahr getBpJ() {
        return bpJ;
    }
    
    public String saveBPJahr(){
        service.updateBPJ(bpJ);
        logger.log(Level.INFO, "BPJahr changed");
        return "success";
    }
    
    public String editBPJahr(BPJahr bJ){
        FacesContext.getCurrentInstance().renderResponse();
        bpJ.setId(bJ.getId());
        bpJ.setBpId(bJ.getBpId());
        bpJ.setJahrId(bJ.getJahrId());
        bpJ.setBetrag(bJ.getBetrag());
        bpJ.setBezDatum(bJ.getBezDatum());
        bpJ.setBefreit(bJ.isBefreit());
        bpJ.setProjekt(bJ.isProjekt());
        return "successEdit";
    }
    
    public String deleteBPJahr(BPJahr bJ) {
        service.removeBPJ(bJ);
        logger.log(Level.INFO, "BPJahr deleted");
        return "successDelete";
    }
    
    public String addBPJahr() {
        service.addBPJ(bpJ);
        logger.log(Level.INFO, "BPJahr added");
        bpJ = new BPJahr();
        return "success";
    }
    
    public String setEmptyBPJahr(){
        bpJ = new BPJahr();
        return "successAdd";
    }
    
}
