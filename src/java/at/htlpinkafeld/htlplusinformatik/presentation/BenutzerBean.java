/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Benutzer;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BaseDao;
import at.htlpinkafeld.htlplusinformatik.infrastructure.BenutzerJdbcDao;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import at.htlpinkafeld.htlplusinformatik.utils.PasswordUtils;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
@ManagedBean
@SessionScoped
public class BenutzerBean {
    public static Logger logger = Logger.getLogger(BenutzerBean.class);
    
    private HtlPlusService service = new HtlPlusService();
    private Benutzer b = new Benutzer();
    private BaseDao<Benutzer> dao = new BenutzerJdbcDao("Benutzer", "UserID");
    private List<Benutzer> benList = service.getBenList();
    
    public BenutzerBean() {
        logger.log(Level.DEBUG, "Created instance of Benutzer");
    }

    public HtlPlusService getService() {
        return service;
    }

    public void setService(HtlPlusService service) {
        this.service = service;
    }

    public Benutzer getBenutzer() {
        return b;
    }
    
    public String saveBenutzer(){
        service.updateBenutzer(b);
        logger.log(Level.INFO, "Benutzer changed");
        return "success";
    }
    
    public String editBenutzer(Benutzer benutzer){
        FacesContext.getCurrentInstance().renderResponse();
        b.setId(benutzer.getId());
        b.setName(benutzer.getName());
        b.setPassword(benutzer.getPassword());
        b.setRight(benutzer.getRight());
        return "successEdit";
    }
    
    public String deleteBenutzer(Benutzer benutzer) {
        service.removeBenutzer(benutzer);
        logger.log(Level.INFO, "Benutzer deleted");
        return "successDelete";
    }
    
    public String addBenutzer(){
        String salt = PasswordUtils.getSalt(30); //salt-Wert (je länger, desto sicherer)
        String mySecurePassword = PasswordUtils.generateSecurePassword(b.getPassword(), salt); //Verschlüsseltes Passwort
        
        Benutzer ben = new Benutzer();
        ben.setId(b.getId());
        ben.setName(b.getName());
        ben.setPassword(mySecurePassword); //gesichertes Passwort
        ben.setSalt(salt);
        ben.setRight(b.getRight());
        
        service.addBenutzer(ben);
        logger.log(Level.INFO, "Benutzer added");
        b = new Benutzer();
        return "success";
    }
    
    public String setEmptyBenutzer(){
        b = new Benutzer();
        return "successAdd";
    }
    
}
