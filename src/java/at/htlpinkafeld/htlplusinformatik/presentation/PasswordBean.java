/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Andor
 */
@ManagedBean
@SessionScoped
public class PasswordBean {

    /**
     * Creates a new instance of PasswordBean
     */
    public PasswordBean() {
    }
    
    public Object cancel() {
        return "cancel";
    }
    
    public Object changePassword() {
        return "changepw";
    }
    
    public Object restrictedChangePassword() {
        return "restrictedchangepw";
    }
    
}
