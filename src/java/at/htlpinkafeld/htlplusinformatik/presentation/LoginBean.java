/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.presentation;

import at.htlpinkafeld.htlplusinformatik.dto.Benutzer;
import at.htlpinkafeld.htlplusinformatik.dto.Benutzer.Right;
import at.htlpinkafeld.htlplusinformatik.service.HtlPlusService;
import at.htlpinkafeld.htlplusinformatik.utils.PasswordUtils;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Daniel Altmann
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static Logger logger = Logger.getLogger(LoginBean.class);

    private HtlPlusService service = new HtlPlusService();

    private String username;
    private String password;
    private Right right;

    private String chechPw;
    private String newPw;
    private String newPw1;

    public LoginBean() {
        logger.log(Level.DEBUG, "Created instance of LoginBean.");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChechPw() {
        return chechPw;
    }

    public void setChechPw(String chechPw) {
        this.chechPw = chechPw;
    }

    public String getNewPw() {
        return newPw;
    }

    public void setNewPw(String newPw) {
        this.newPw = newPw;
    }

    public String getNewPw1() {
        return newPw1;
    }

    public void setNewPw1(String newPw1) {
        this.newPw1 = newPw1;
    }

    public void login() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = null;

        Benutzer b = null;

        b = service.selectBenutzer(username);

        String salt = null;
        String providedPassword = null;
        String securePassword = null;

        if (b != null) {
            salt = b.getSalt();
            providedPassword = this.password;
            securePassword = b.getPassword();
        }

        boolean passwordsMatch = PasswordUtils.verifyUserPassword(providedPassword, securePassword, salt);

        if (passwordsMatch) {
            if (b.getRight() == Right.ADMIN) {
                this.right = Right.ADMIN;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);

                context.getExternalContext().redirect("/HTL_Plus_Informatik/faces/restricted/AdminStart.xhtml");
                context.getExternalContext().getFlash().setKeepMessages(true);

                HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
                session.setAttribute("loggedIn", new Object());
            } else {
                this.right = Right.USER;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);

                context.getExternalContext().redirect("/HTL_Plus_Informatik/faces/MitgliedStart.xhtml");
                context.getExternalContext().getFlash().setKeepMessages(true);

                HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
                session.setAttribute("loggedIn", new Object());
            }
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error!", "Username oder Passwort falsch!");
            logger.log(Level.INFO, "logged in");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        logger.log(Level.INFO, "logged in");

    }

    public Object logout() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message;

        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.removeAttribute("loggedIn");
        session.invalidate();

        message = new FacesMessage(FacesMessage.SEVERITY_INFO, username, " logged out!");

        context.getExternalContext().getFlash().setKeepMessages(true);

        //PrimeFaces.current().dialog().showMessageDynamic(message);
        FacesContext.getCurrentInstance().addMessage(null, message);
        logger.log(Level.INFO, "logged out");
        return "logout";
    }

    public void changePassword() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = null;

        Benutzer b = service.selectBenutzer(this.username);

        String salt = null;
        String providedPassword = null;
        String securePassword = null;

        if (b != null) {
            salt = b.getSalt();
            providedPassword = this.chechPw;
            securePassword = b.getPassword();
        }

        boolean passwordsMatch = PasswordUtils.verifyUserPassword(providedPassword, securePassword, salt);
        boolean newPasswordsMatch = true;
        boolean newPasswordOK = true;
        boolean newPasswordIsOldOne = false;

        if (!this.newPw.equals(this.newPw1)) {
            newPasswordsMatch = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error!", "Passwörter stimmen nicht überein!");
        }

        if (this.newPw.length() < 5 || this.newPw.length() > 12 || !this.newPw.matches(".*\\d+.*")) {
            newPasswordOK = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error!", "Das Passwort sollte "
                    + "5 bis 12 Zeichen lang sein und mindestens einen Nummer beinhalten.");
        }
        if (this.newPw.equals(this.chechPw)) {
            newPasswordIsOldOne = true;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error!", "Neues Password sollte mit dem alten"
                    + "nicht übereinstimmen!");
        }

        if (passwordsMatch && newPasswordsMatch && newPasswordOK && !newPasswordIsOldOne) {
            String mySecurePassword = PasswordUtils.generateSecurePassword(this.newPw, salt);

            b.setPassword(mySecurePassword);
            service.updateBenutzer(b);
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success!", "Passwort erfolgreich geändert!"
                    + "Bitte melden Sie sich erneut an mit Ihrem neuen Passwort!");
            context.getExternalContext().redirect("/HTL_Plus_Informatik/faces/Login.xhtml");
            logout();
        } else if (!passwordsMatch && newPasswordsMatch && newPasswordOK && !newPasswordIsOldOne) {
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error!", "Altes Passwort ist falsch!");
        }

        context.getExternalContext().getFlash().setKeepMessages(true);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
