/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class Jahr implements Identifiable{
    public static Logger logger = Logger.getLogger(Jahr.class);
    
    private int jahrid;
    private int jahrbez;
    
    public Jahr (){
        this.jahrid = -1;
        logger.log(Level.DEBUG, "Created empty Jahr");
    }

    public Jahr(int jahrbez) {
        this();
        this.jahrbez = jahrbez;
        logger.log(Level.DEBUG, "Created Jahr");
    }
    
    

    @Override
    public int getId() {
        return this.jahrid;
    }

    @Override
    public void setId(int id) {
        this.jahrid = id;
    }

    public int getJahrid() {
        return jahrid;
    }

    public void setJahrid(int jahrid) {
        this.jahrid = jahrid;
    }

    public int getJahrbez() {
        return jahrbez;
    }

    public void setJahrbez(int jahrbez) {
        this.jahrbez = jahrbez;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.jahrid;
        hash = 41 * hash + this.jahrbez;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jahr other = (Jahr) obj;
        if (this.jahrid != other.jahrid) {
            return false;
        }
        if (this.jahrbez != other.jahrbez) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Jahr{" + "jahrid=" + jahrid + ", jahrbez=" + jahrbez + '}';
    }
    
    
}
