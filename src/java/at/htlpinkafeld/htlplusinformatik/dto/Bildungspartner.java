/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import java.util.Objects;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class Bildungspartner implements Identifiable {
    public static Logger logger = Logger.getLogger(Bildungspartner.class);
    
    private int num;
    private String firma;
    //private String ansprPers;
    private String addr;
    private String plz;
    private String ort;
    private boolean isAbsolvent;
    private String website;
    private String fax = "kein Fax";
    private String email;
    //private String tel;
    private boolean isBezahlt;
    private int betrag = 0;
    private String anmerk = " - ";
    private String eDatum = " - ";
    private String aDatum = " - ";

    public Bildungspartner() {
        this.num = -1;
        logger.log(Level.DEBUG, "Created empty Bildungspartner");
    }

    public Bildungspartner(String firma, String addr, String plz, String ort, boolean isAbsolvent, String website, String fax, String email, int betrag, boolean isBezahlt, String anm, String eDat, String aDat) {
        this();
        this.firma = firma;
        //this.ansprPers = ansprPers;
        this.addr = addr;
        this.plz = plz;
        this.ort = ort;
        this.isAbsolvent = isAbsolvent;
        this.website = website;
        this.fax = fax;
        this.email = email;
        this.anmerk = anm;
        this.eDatum = eDat;
        this.aDatum = aDat;
        this.isBezahlt = isBezahlt;
        this.betrag = betrag;
        logger.log(Level.DEBUG, "Created Bildungspartner");
    }

    @Override
    public int getId() {
        return this.num;
    }

    @Override
    public void setId(int id) {
        this.num = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }
    
    

    /*public String getAnsprPers() {
        return ansprPers;
    }

    public void setAnsprPers(String ansprPers) {
        this.ansprPers = ansprPers;
    }*/

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public boolean isAbsolvent() {
        return isAbsolvent;
    }

    public void setAbsolvent(boolean isAbsolvent) {
        this.isAbsolvent = isAbsolvent;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    
    public boolean isBezahlt() {
        return isBezahlt;
    }

    public void setBezahlt(boolean isBezahlt) {
        this.isBezahlt = isBezahlt;
    }

    public int getBetrag() {
        return betrag;
    }

    public void setBetrag(int betrag) {
        this.betrag = betrag;
    }

    public String getAnmerk() {
        return anmerk;
    }

    public void setAnmerk(String anmerk) {
        this.anmerk = anmerk;
    }

    public boolean isIsAbsolvent() {
        return isAbsolvent;
    }

    public void setIsAbsolvent(boolean isAbsolvent) {
        this.isAbsolvent = isAbsolvent;
    }

    public String geteDatum() {
        return eDatum;
    }

    public void seteDatum(String eDatum) {
        this.eDatum = eDatum;
    }

    public String getaDatum() {
        return aDatum;
    }

    public void setaDatum(String aDatum) {
        this.aDatum = aDatum;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.num;
        hash = 97 * hash + Objects.hashCode(this.firma);
        hash = 97 * hash + Objects.hashCode(this.addr);
        hash = 97 * hash + Objects.hashCode(this.plz);
        hash = 97 * hash + Objects.hashCode(this.ort);
        hash = 97 * hash + (this.isAbsolvent ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.website);
        hash = 97 * hash + Objects.hashCode(this.fax);
        hash = 97 * hash + Objects.hashCode(this.anmerk);
        hash = 97 * hash + Objects.hashCode(this.eDatum);
        hash = 97 * hash + Objects.hashCode(this.aDatum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bildungspartner other = (Bildungspartner) obj;
        if (this.num != other.num) {
            return false;
        }
        if (this.isAbsolvent != other.isAbsolvent) {
            return false;
        }
        if (!Objects.equals(this.firma, other.firma)) {
            return false;
        }
        if (!Objects.equals(this.addr, other.addr)) {
            return false;
        }
        if (!Objects.equals(this.plz, other.plz)) {
            return false;
        }
        if (!Objects.equals(this.ort, other.ort)) {
            return false;
        }
        if (!Objects.equals(this.website, other.website)) {
            return false;
        }
        if (!Objects.equals(this.fax, other.fax)) {
            return false;
        }
        if (!Objects.equals(this.anmerk, other.anmerk)) {
            return false;
        }
        if (!Objects.equals(this.eDatum, other.eDatum)) {
            return false;
        }
        if (!Objects.equals(this.aDatum, other.aDatum)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "Bildungspartner{" + "num=" + num + ", firma=" + firma + ", addr=" + addr + ", plz=" + plz + ", ort=" + ort + ", isAbsolvent=" + isAbsolvent + ", website=" + website + ", fax=" + fax + ", anmerk=" + anmerk + ", eDatum=" + eDatum + ", aDatum=" + aDatum + '}';
    }
    
            
}
