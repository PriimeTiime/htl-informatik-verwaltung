/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import static at.htlpinkafeld.htlplusinformatik.dto.Bildungspartner.logger;
import java.util.Objects;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class Mitglied implements Identifiable{
    public static Logger logger = Logger.getLogger(Mitglied.class);
    
    private int num;
    private String vname;
    private String nname;
    private String funktion;
    private String email;
    private String telnr;
    
    public Mitglied(){
        this.num = -1;
        logger.log(Level.DEBUG, "Created empty Mitglied");
    }
    
    
    public Mitglied(String vname, String nname, String funktion, String email, String telnr){
        this();
        this.vname = vname;
        this.nname = nname;
        this.funktion = funktion;
        this.email = email;
        this.telnr = telnr;
        logger.log(Level.DEBUG, "Created Mitglied");
    }

    @Override
    public int getId() {
        return this.num;
    }

    @Override
    public void setId(int id) {
        this.num = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getVname() {
        return vname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getFunktion() {
        return funktion;
    }

    public void setFunktion(String funktion) {
        this.funktion = funktion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelnr() {
        return telnr;
    }

    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.num;
        hash = 67 * hash + Objects.hashCode(this.vname);
        hash = 67 * hash + Objects.hashCode(this.nname);
        hash = 67 * hash + Objects.hashCode(this.funktion);
        hash = 67 * hash + Objects.hashCode(this.email);
        hash = 67 * hash + Objects.hashCode(this.telnr);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mitglied other = (Mitglied) obj;
        if (this.num != other.num) {
            return false;
        }
        if (!Objects.equals(this.vname, other.vname)) {
            return false;
        }
        if (!Objects.equals(this.nname, other.nname)) {
            return false;
        }
        if (!Objects.equals(this.funktion, other.funktion)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.telnr, other.telnr)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mitglied{" + "num=" + num + ", vname=" + vname + ", nname=" + nname + ", funktion=" + funktion + ", email=" + email + ", telnr=" + telnr + '}';
    }
    
    
}
