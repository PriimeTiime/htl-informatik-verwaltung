/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import java.util.Objects;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class Benutzer implements Identifiable {
    public static Logger logger = Logger.getLogger(Benutzer.class);
    
    public enum Right {
        ADMIN, USER
    };

    private int userid;
    private String name;
    private String password;
    private String salt;
    private Right right;
    
    public Benutzer() {
        this.userid = -1;
        logger.log(Level.DEBUG, "Created empty Benutzer");
    }

    public Benutzer(String name, String password, String salt, Right right) {
        this();
        this.name = name;
        this.password = password;
        this.salt = salt;
        this.right = right;
        logger.log(Level.DEBUG, "Created Benutzer");
    }

    @Override
    public int getId() {
        return this.userid;
    }

    @Override
    public void setId(int id) {
        this.userid = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Right getRight() {
        return this.right;
    }

    public void setRight(Right right) {
        this.right = right;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.userid;
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.password);
        hash = 79 * hash + Objects.hashCode(this.salt);
        hash = 79 * hash + Objects.hashCode(this.right);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Benutzer other = (Benutzer) obj;
        if (this.userid != other.userid) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.salt, other.salt)) {
            return false;
        }
        if (this.right != other.right) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Benutzer{" + "userid=" + userid + ", name=" + name + ", password=" + password + ", salt=" + salt + ", right=" + right + '}';
    }
    
}
