/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import java.sql.Date;
import java.util.Objects;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class BPJahr implements Identifiable {
    public static Logger logger = Logger.getLogger(BPJahr.class);
    
    private int bpJahrId;
    private int bpId; // BildungspartnerID FK
    private int jahrId; // JahrID FK
    private double betrag;
    private Date bezDatum;
    private boolean befreit;
    private boolean projekt;
    
    
    public BPJahr() {
        this.bpJahrId = -1;
        logger.log(Level.DEBUG, "Created empty Bildungspartner");
    }

    public BPJahr(int bpId, int jahrId, double betrag, Date bezDatum, boolean befreit, boolean projekt) {
        this();
        this.bpId = bpId;
        this.jahrId = jahrId;
        this.betrag = betrag;
        this.bezDatum = bezDatum;
        this.befreit = befreit;
        this.projekt = projekt;
        logger.log(Level.DEBUG, "Created BP_Jahr");
    }
    
    

    @Override
    public int getId() {
        return this.bpJahrId;
    }

    @Override
    public void setId(int id) {
        this.bpJahrId = id;
    }

    public int getBpJahrId() {
        return bpJahrId;
    }

    public void setBpJahrId(int bpJahrId) {
        this.bpJahrId = bpJahrId;
    }

    public int getBpId() {
        return bpId;
    }

    public void setBpId(int bpId) {
        this.bpId = bpId;
    }

    public int getJahrId() {
        return jahrId;
    }

    public void setJahrId(int jahrId) {
        this.jahrId = jahrId;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    public Date getBezDatum() {
        return bezDatum;
    }

    public void setBezDatum(Date bezDatum) {
        this.bezDatum = bezDatum;
    }

    public boolean isBefreit() {
        return befreit;
    }

    public void setBefreit(boolean befreit) {
        this.befreit = befreit;
    }

    public boolean isProjekt() {
        return projekt;
    }

    public void setProjekt(boolean projekt) {
        this.projekt = projekt;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.bpJahrId;
        hash = 67 * hash + this.bpId;
        hash = 67 * hash + this.jahrId;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.betrag) ^ (Double.doubleToLongBits(this.betrag) >>> 32));
        hash = 67 * hash + Objects.hashCode(this.bezDatum);
        hash = 67 * hash + (this.befreit ? 1 : 0);
        hash = 67 * hash + (this.projekt ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BPJahr other = (BPJahr) obj;
        if (this.bpJahrId != other.bpJahrId) {
            return false;
        }
        if (this.bpId != other.bpId) {
            return false;
        }
        if (this.jahrId != other.jahrId) {
            return false;
        }
        if (Double.doubleToLongBits(this.betrag) != Double.doubleToLongBits(other.betrag)) {
            return false;
        }
        if (this.befreit != other.befreit) {
            return false;
        }
        if (this.projekt != other.projekt) {
            return false;
        }
        if (!Objects.equals(this.bezDatum, other.bezDatum)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BP_Jahr{" + "bpJahrId=" + bpJahrId + ", bpId=" + bpId + ", jahrId=" + jahrId + ", betrag=" + betrag + ", bezDatum=" + bezDatum + ", befreit=" + befreit + ", projekt=" + projekt + '}';
    }
    
    
    
}
