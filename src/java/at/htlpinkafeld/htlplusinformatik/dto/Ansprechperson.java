/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.dto;

import java.util.Objects;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class Ansprechperson implements Identifiable {
    public static Logger logger = Logger.getLogger(Ansprechperson.class);
    
    private int id;
    private String name;
    private String abt;
    private String email;
    private String tel;
    private int bpId;
    private String zuständikeit;

    public Ansprechperson() {
        this.id = -1;
        logger.log(Level.DEBUG, "Created empty Ansprechperson object");
    }

    public Ansprechperson(String name, String abt, String email, String tel, int bpId, String zust) {
        this();
        this.name = name;
        this.abt = abt;
        this.email = email;
        this.tel = tel;
        this.bpId = bpId;
        this.zuständikeit = zust;
        logger.log(Level.DEBUG, "Created Ansprechperson object");
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbt() {
        return abt;
    }

    public void setAbt(String abt) {
        this.abt = abt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getBpId() {
        return bpId;
    }

    public void setBpId(int bpId) {
        this.bpId = bpId;
    }

    public String getZuständikeit() {
        return zuständikeit;
    }

    public void setZuständikeit(String zuständikeit) {
        this.zuständikeit = zuständikeit;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.abt);
        hash = 71 * hash + Objects.hashCode(this.email);
        hash = 71 * hash + Objects.hashCode(this.tel);
        hash = 71 * hash + this.bpId;
        hash = 71 * hash + Objects.hashCode(this.zuständikeit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ansprechperson other = (Ansprechperson) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.bpId != other.bpId) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.abt, other.abt)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.tel, other.tel)) {
            return false;
        }
        if (!Objects.equals(this.zuständikeit, other.zuständikeit)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Ansprechperson{" + "id=" + id + ", name=" + name + ", abt=" + abt + ", email=" + email + ", tel=" + tel + ", bpId=" + bpId + ", zust\u00e4ndikeit=" + zuständikeit + '}';
    }
    
}
