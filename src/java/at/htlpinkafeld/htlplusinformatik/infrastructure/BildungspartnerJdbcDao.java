/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Bildungspartner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class BildungspartnerJdbcDao extends BaseJdbcDao<Bildungspartner> implements BildungspartnerDao {
    private static Logger logger = Logger.getLogger(BildungspartnerJdbcDao.class);

    //select count(*) from Benutzer where username=? and password=?
    public BildungspartnerJdbcDao(String tablename, String pkName) {
        super("Bildungspartner", "BPNum"); //tablename, primarykey
    }
    

    @Override
    protected Bildungspartner getPojoFromResultSet(ResultSet result) throws SQLException {
        Bildungspartner bp = new Bildungspartner(result.getString("FName"), 
                result.getString("Adresse"), result.getString("PLZ"),
                result.getString("Ort"), result.getBoolean("IsAbsolvent"), 
                result.getString("Website"), result.getString("Fax"), result.getString("Email"),
                result.getInt("Betrag"), result.getBoolean("isBezahlt"), result.getString("Anmerk"), 
                result.getString("Eintrittsdatum"), result.getString("Austrittsdatum"));
        
        bp.setId(result.getInt(this.getPkName()));
        
        logger.log(Level.DEBUG, "getPojoFromResultSet Bildungspartner");
        return bp;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection conn, Bildungspartner bp) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET FName = ?, Adresse = ?, "
                + "PLZ = ?, Ort = ?, IsAbsolvent = ?, Website = ?,"
                + "Fax = ?, Email = ?, Betrag=?, Bezahlt = ? ,Eintrittsdatum = ?"
                + ", Austrittsdatum = ? , Anmerk = ? WHERE " + getPkName() + " = ? "; //aufpassen wegen teilweise-updaten 
                                                                          //(wenn ein neues objekt instanziiert wird,
                                                                          //werden die daten, die nicht updatet werden
                                                                          //leer gesetzt!!
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setInt(11, bp.getId());
        stmt.setString(1, bp.getFirma());
        //stmt.setString(2, bp.getAnsprPers());
        stmt.setString(2, bp.getAddr());
        stmt.setString(3, bp.getPlz());
        stmt.setString(4, bp.getOrt());
        stmt.setBoolean(5, bp.isAbsolvent());
        stmt.setString(6, bp.getWebsite());
        stmt.setString(7, bp.getFax());
        stmt.setString(8, bp.getEmail());
        stmt.setInt(9, bp.getBetrag());
        stmt.setBoolean(10, bp.isBezahlt());
        stmt.setString(11, bp.geteDatum());
        stmt.setString(12, bp.getaDatum());
        stmt.setString(13, bp.getAnmerk());
        
        logger.log(Level.DEBUG, "getUpdateStatement Bildungspartner");
        return stmt;    
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection conn, Bildungspartner bp) throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (FName, Adresse, PLZ, Ort, IsAbsolvent, Website, Fax, Email, Betrag, isBezahlt, Eintrittsdatum, Austrittsdatum, Anmerk) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        stmt.setString(1, bp.getFirma());
        //stmt.setString(2, bp.getAnsprPers());
        stmt.setString(2, bp.getAddr());
        stmt.setString(3, bp.getPlz());
        stmt.setString(4, bp.getOrt());
        stmt.setBoolean(5, bp.isAbsolvent());
        stmt.setString(6, bp.getWebsite());
        stmt.setString(7, bp.getFax());
        stmt.setString(8, bp.getEmail());
        stmt.setInt(9, bp.getBetrag());
        stmt.setBoolean(10, bp.isBezahlt());
        stmt.setString(11, bp.geteDatum());
        stmt.setString(12, bp.getaDatum());
        stmt.setString(13, bp.getAnmerk());
        
        logger.log(Level.DEBUG, "getInsertStatement Bildungspartner");
        return stmt;    
    }

}

