/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class ConnectionManager {
    private static Logger logger = Logger.getLogger(ConnectionManager.class);
    private static ConnectionManager instance = null;
    private DataSource dsIntern = null;
    private DataSource dsExtern = null;
    private DataSource dsLocal = null;

    public static synchronized ConnectionManager getInstance() {
        if (instance == null) {
            instance = new ConnectionManager();
        }

        logger.log(Level.DEBUG, "Connection instance retrieved.");
        return instance;
    }

    private ConnectionManager() {
        try {
            InitialContext ctx = new javax.naming.InitialContext();
            dsIntern = (DataSource) ctx.lookup("java:comp/env/" + "mysql/htlplus_intern");
            dsExtern = (DataSource) ctx.lookup("java:comp/env/" + "mysql/htlplus_extern");
            dsLocal = (DataSource) ctx.lookup("java:comp/env/" + "mysql/htlplus_local");
        } catch (NamingException ex) {
            logger.log(Level.FATAL, "Could not create and instance of ConnectionManager.");
        }
    }

    public Connection getConnection() {
        try {
            return dsExtern.getConnection();
        } catch (SQLException e) {
            try {
                return dsIntern.getConnection();
            } catch (SQLException ex) {
                try {
                    return dsLocal.getConnection();
                } catch (SQLException exe) {
                    logger.log(Level.FATAL, "Could not retrieve connection.");
                }
            }
        }
        return null;
    }
}

//private final String driverName = "com.mysql.jdbc.Driver";
//    private final String connectionUrl = "jdbc:mysql://10.2.1.16:3306/HTLplusBP_db";
//            
//    private final String userName = "HTLplusBP";
//    private final String userPass = "Fidelio1978@!";
//
//    private static volatile ConnectionManager instance = null;
//    private Connection conn;
//    
//    private ConnectionManager() {
//        /*try {
//            Class.forName(driverName);
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
//        }*/
//    }
//    
//    public static synchronized ConnectionManager getInstance() {
//        if(instance == null)
//            instance = new ConnectionManager();
//        return instance;
//    }
//
//    public Connection getConnection() {
//        try {
//            conn = DriverManager.getConnection(connectionUrl, userName, userPass);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return conn;
//    }
//
//    public void closeConnection() {
//        try {
//            this.conn.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
