/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.BPJahr;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class BPJahrJdbcDao extends BaseJdbcDao<BPJahr> implements BPJahrDao{
    private static Logger logger = Logger.getLogger(BPJahrJdbcDao.class);
    
    public BPJahrJdbcDao(String tablename, String pkName) {
        super("BP_Jahr", "BPJahrID"); //tablename, primarykey
    }

    @Override
    protected BPJahr getPojoFromResultSet(ResultSet result) throws SQLException {
        BPJahr bpJ = new BPJahr(result.getInt("BPID"), 
                result.getInt("JahrID"), result.getDouble("Betrag"),
                result.getDate("BezahlDat"), result.getBoolean("Zahlungsbefreit"), 
                result.getBoolean("Projekt"));
        
        bpJ.setId(result.getInt(this.getPkName()));
        
        logger.log(Level.DEBUG, "getPojoFromResultSet BPJahr");
        return bpJ;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection conn, BPJahr bpJ) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET BPID = ?, JahrID = ?, "
                + "Betrag = ?, BezahlDat = ?, Zahlungsbefreit = ?, Projekt = ?,"
                + " WHERE " + getPkName() + " = ? ";
        
        PreparedStatement stmt = conn.prepareStatement(sql);
        
        stmt.setInt(7, bpJ.getId());
        stmt.setInt(1, bpJ.getBpId());
        stmt.setInt(2, bpJ.getJahrId());
        stmt.setDouble(3, bpJ.getBetrag());
        stmt.setDate(4, bpJ.getBezDatum());
        stmt.setBoolean(5, bpJ.isBefreit());
        stmt.setBoolean(6, bpJ.isProjekt());
        
        logger.log(Level.DEBUG, "getUpdateStatement BPJahr");
        return stmt;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection conn, BPJahr bpJ) throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (BPID, JahrID, Betrag, BezahlDat, Zahlungsbefreit, Projekt) VALUES (?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        stmt.setInt(1, bpJ.getId());
        stmt.setInt(2, bpJ.getBpId());
        stmt.setInt(3, bpJ.getJahrId());
        stmt.setDouble(4, bpJ.getBetrag());
        stmt.setDate(5, bpJ.getBezDatum());
        stmt.setBoolean(6, bpJ.isBefreit());
        stmt.setBoolean(7, bpJ.isProjekt());
        
        logger.log(Level.DEBUG, "getInsertStatement BPJahr");
        return stmt;
    }
    
    
}
