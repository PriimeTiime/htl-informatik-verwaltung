/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Mitglied;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class MitgliedJdbcDao extends BaseJdbcDao<Mitglied> implements MitgliedDao{
    private static Logger logger = Logger.getLogger(MitgliedJdbcDao.class);

    public MitgliedJdbcDao(String tablename, String pkName) {
        super("Mitglieder", "MitgliedID"); //tablename, primarykey
    }

    @Override
    protected Mitglied getPojoFromResultSet(ResultSet result) throws SQLException {
        Mitglied m = new Mitglied(result.getString("Vorname"),result.getString("Nachname"), 
        result.getString("Funktion"),result.getString("EMail"),result.getString("TelNr"));
        
        m.setId(result.getInt(this.getPkName()));
        
        logger.log(Level.DEBUG, "getPojoFromResultSet Mitglied");
        return m;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection conn, Mitglied m) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET Vorname = ?, Nachname = ?, "
                + "Funktion = ?, EMail = ?, TelNr = ? WHERE " + getPkName() + " = ? ";
        
         PreparedStatement stmt = conn.prepareStatement(sql);
         
        stmt.setInt(6, m.getId());
        stmt.setString(1, m.getVname());
        stmt.setString(2, m.getNname());
        stmt.setString(3, m.getFunktion());
        stmt.setString(4, m.getEmail());
        stmt.setString(5, m.getTelnr());
        
        logger.log(Level.DEBUG, "getUpdateStatement Mitglied");
        return stmt;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection conn, Mitglied m) throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (Vorname, Nachname, Funktion, EMail, TelNr) VALUES (?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        stmt.setString(1, m.getVname());
        stmt.setString(2, m.getNname());
        stmt.setString(3, m.getFunktion());
        stmt.setString(4, m.getEmail());
        stmt.setString(5, m.getTelnr());
        
        logger.log(Level.DEBUG, "getInsertStatement Mitglied");
        return stmt;
    }
    
}
