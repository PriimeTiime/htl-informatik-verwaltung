/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Mitglied;

/**
 *
 * @author danie
 */
public interface MitgliedDao extends BaseDao<Mitglied>{
    
}
