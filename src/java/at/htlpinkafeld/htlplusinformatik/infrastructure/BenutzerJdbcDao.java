/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Benutzer;
import at.htlpinkafeld.htlplusinformatik.dto.Benutzer.Right;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class BenutzerJdbcDao extends BaseJdbcDao<Benutzer> implements BenutzerDao {

    private static Logger logger = Logger.getLogger(BenutzerJdbcDao.class);

    public BenutzerJdbcDao(String tablename, String pkName) {
        super("Benutzer", "UserID"); //tablename, primarykey
    }

    @Override
    protected Benutzer getPojoFromResultSet(ResultSet result) throws SQLException {
        Benutzer user = new Benutzer(result.getString("UserName"),
                result.getString("Password"), result.getString("Salt"),
                Right.valueOf(result.getString("Rechte")));

        user.setId(result.getInt(this.getPkName()));

        logger.log(Level.DEBUG, "getPojoFromResultSet Benutzer");
        return user;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection conn, Benutzer user) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET UserName = ?, Password = ?, "
                + "Rechte = ? WHERE " + getPkName() + " = ? ";

        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setInt(4, user.getId());
        stmt.setString(1, user.getName());
        stmt.setString(2, user.getPassword());
        stmt.setString(3, user.getRight().toString());

        logger.log(Level.DEBUG, "getUpdateStatement Benutzer");
        return stmt;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection conn, Benutzer user) throws SQLException {
        String sql = "INSERT INTO " + getTablename() + " (UserName, Password, Salt, Rechte) VALUES (?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, user.getName());
        stmt.setString(2, user.getPassword());
        stmt.setString(3, user.getSalt());
        stmt.setString(4, user.getRight().toString());

        logger.log(Level.DEBUG, "getInsertStatement Benutzer");
        return stmt;
    }

    @Override
    public Benutzer selectBenutzer(String username) {
        Benutzer b = null;
        String sql = "SELECT * FROM " + getTablename()
                + " WHERE UserName=?;";

        try (Connection conn = ConnectionManager.getInstance().getConnection();
                PreparedStatement stmt = generateStatement(conn, sql, username);
                ResultSet result = stmt.executeQuery()) {

            if (result.next()) {
                b = getPojoFromResultSet(result);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(BenutzerJdbcDao.class.getName()).log(Level.ERROR, null, ex);
        }

        return b;
    }
    
    protected PreparedStatement generateStatement(Connection con, String sql, String username) throws SQLException{
        PreparedStatement ps = con.prepareStatement(sql);
        
        ps.setString(1, username);
        
        return ps;
    }
}
