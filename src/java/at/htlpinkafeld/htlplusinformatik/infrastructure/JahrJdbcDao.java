/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Jahr;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author danie
 */
public class JahrJdbcDao extends BaseJdbcDao<Jahr> implements JahrDao{
    private static Logger logger = Logger.getLogger(JahrJdbcDao.class);

    public JahrJdbcDao(String tablename, String pkName) {
        super("Jahr", "JahrID");
    }

    @Override
    protected Jahr getPojoFromResultSet(ResultSet result) throws SQLException {
        Jahr j = new Jahr(result.getInt("Bezeichnung_Jahr"));
        
        j.setId(result.getInt(this.getPkName()));
        j.setJahrbez(result.getInt("Bezeichnung_Jahr"));
        
        logger.log(Level.DEBUG, "getPojoFromResultSet Jahr");
        return j;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Jahr j) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET Bezeichnung_Jahr"
                + " = ? WHERE " + getPkName() + " = ? "; //aufpassen wegen teilweise-updaten 
                                                                          //(wenn ein neues objekt instanziiert wird,
                                                                          //werden die daten, die nicht updatet werden
                                                                          //leer gesetzt!!
        PreparedStatement stmt = c.prepareStatement(sql);

        stmt.setInt(2, j.getId());
        stmt.setInt(1, j.getJahrbez());
        
        
        logger.log(Level.DEBUG, "getUpdateStatement Jahr");
        return stmt;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Jahr j) throws SQLException {
       String sql = "INSERT INTO " + getTablename() + " (Bezeichnung_Jahr) VALUES (?)";
        PreparedStatement stmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        stmt.setInt(1, j.getJahrbez());
        
        logger.log(Level.DEBUG, "getInsertStatement Jahr");
        return stmt;
    }
    
}
