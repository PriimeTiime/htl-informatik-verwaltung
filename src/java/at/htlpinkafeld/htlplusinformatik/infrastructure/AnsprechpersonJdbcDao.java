/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import at.htlpinkafeld.htlplusinformatik.dto.Ansprechperson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Andor
 */
public class AnsprechpersonJdbcDao extends BaseJdbcDao<Ansprechperson> implements AnsprechpersonDao {
    private static Logger logger = Logger.getLogger(AnsprechpersonJdbcDao.class);

    public AnsprechpersonJdbcDao(String tablename, String pkName) {
        super("Ansprechperson", "AnsPersID"); //tablename, primarykey
    }

    @Override
    protected Ansprechperson getPojoFromResultSet(ResultSet result) throws SQLException {
        Ansprechperson ans = new Ansprechperson(result.getString("Name"), 
                result.getString("Abteilung"), result.getString("EMail"),
                result.getString("TelNr"),result.getInt("BildungspartnerID"),
                result.getString("Zuständigkeit"));
        
        ans.setId(result.getInt(this.getPkName()));
        ans.setName(result.getString("Name"));
        ans.setAbt(result.getString("Abteilung"));
        ans.setEmail(result.getString("EMail"));
        ans.setTel(result.getString("TelNr"));
        ans.setBpId(result.getInt("BildungspartnerID"));
        ans.setZuständikeit(result.getString("Zuständigkeit"));
        
        logger.log(Level.DEBUG, "getPojoFromResultSet");
        return ans;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection conn, Ansprechperson ans) throws SQLException {
        String sql = "UPDATE " + getTablename() + " SET Name = ?, Abteilung = ?, "
                + "EMail = ?, TelNr = ? , BildungspartnerID = ?, Zuständigkeit = ? WHERE " + getPkName() + " = ? "; 
        
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setInt(7, ans.getId());
        stmt.setString(1, ans.getName());
        stmt.setString(2, ans.getAbt());
        stmt.setString(3, ans.getEmail());
        stmt.setString(4, ans.getTel());
        stmt.setInt(5, ans.getBpId());
        stmt.setString(6, ans.getZuständikeit());
        
        logger.log(Level.DEBUG, "getUpdateStatement");
        return stmt;    
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection conn, Ansprechperson ans) throws SQLException {
    String sql = "INSERT INTO " + getTablename() + " (Name, Abteilung,"
            + "EMail, TelNr) VALUES (?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        stmt.setString(1, ans.getName());
        stmt.setString(2, ans.getAbt());
        stmt.setString(3, ans.getEmail());
        stmt.setString(4, ans.getTel());
        stmt.setInt(5, ans.getBpId());
        stmt.setString(6, ans.getZuständikeit());
        
        logger.log(Level.DEBUG, "getInsertStatement");
        
        return stmt;    
    }
}