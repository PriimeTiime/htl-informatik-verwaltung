/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.htlplusinformatik.infrastructure;

import java.util.List;

/**
 *
 * @author Andor
 */
public interface BaseDao<T> {
    public List<T> list();
    public T read(int index);
    public void insert(T t);
    public void update(T t);
    public void delete(T t);
}
